<?php
include("./DB.php");
include("./Statistic.php");

$db = new DB();
$_POST = json_decode(file_get_contents('php://input'), true);
$url = $_POST["url"];
$command = $_POST["command"];
$element = $_POST["element"];


$statistic = new Statistic($db);

switch ($command) {
    case 'check_same_request':
        // Cheсk if the same request was made less than 5 minutes ago.
        $checkSameRequest = $statistic->checkSameRequest($url, $element);
        echo json_encode($checkSameRequest);
        break;
    case 'check_request':
        $id_request = $_POST["id_request"];
        $count_elements_page = $_POST['count_elements_page'];
        // Set statistics datа for website
        $statistic->setClientData($url, $element, $id_request, $count_elements_page);
        // Get statistics datа for website
        $statisticForPage = $statistic->clientsStatistic($element, $url);
        echo json_encode($statisticForPage);
        break;
    case 'get_statistic':
        $statisticForPage = $statistic->clientsStatistic($element, $url);
        // Send statistics datа for website
        echo json_encode($statisticForPage);
        break;
}







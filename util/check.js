// We check the correctness of filling the form
function checkForm(form) {

    if (form.url.value == "") {
        document.getElementById("error_message").innerHTML = "URL field is not filled";
        return false;
    } else if (form.element.value == "") {
        document.getElementById("error_message").innerHTML = "Element field is not filled";
        return false;
    } else if (!validURL(form.url.value)) {
        document.getElementById("error_message").innerHTML = "Please enter a valid URL";
        return false;
    }
    return true;
}

// Make a request to the server to obtain statistical data
function getStatistic(form) {
    var xhttpr = new XMLHttpRequest();
    var getStatistic = {
        "command": "get_statistic",
        "element": form.element.value,
        "url": form.url.value
    };
// Convert a JavaScript object getStatistic into a string
    var str = JSON.stringify(getStatistic);
    // Specifies the type of request
    xhttpr.open("POST", "/colnect/counter.php", true);
    // Add HTTP headers to the request
    xhttpr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhttpr.send(str);

    getStatisticForUrl(xhttpr);
}

// Get and Insert statistical data from the server into html document
function getStatisticForUrl(xhttpr) {

    xhttpr.onreadystatechange = function () {
        // When readyState property is 4 and the status property is 200, the response is ready:
        if (xhttpr.readyState == 4 && xhttpr.status == 200) {
            // Use the JavaScript function JSON.parse() to convert text into a JavaScript object
            var statisticForPage = JSON.parse(xhttpr.responseText);
            var statisticReport = 'General Statistics' + '<br>';
            statisticReport += statisticForPage.countCheckedUrlsOfDomain + ' different URLs from ' +
                statisticForPage.domain + ' have been fetched' + '<br>';
            statisticReport += 'Average fetch time from ' + statisticForPage.domain +
                ' during the last 24 hours is ' + statisticForPage.averagePageFetchTime + ' ms' + '<br>';
            statisticReport += 'There was a total of ' + statisticForPage.totalCountElementsDomain + ' < ' +
                statisticForPage.element + ' >' + ' elements from ' + statisticForPage.domain + '<br>';
            statisticReport += 'Total of ' + statisticForPage.totalCountElements + ' < ' +
                statisticForPage.element + ' > ' + ' elements counted in all requests ever made.';

            document.getElementById("statisticReport").innerHTML = statisticReport;
        }
    };
}

function timeFormatting(timeInSec) {

    var options = {
        year: "numeric",
        month: "2-digit",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit"
    };
    var date = new Date();
    date.setTime(timeInSec * 1000);
    var dateStr = date.toLocaleString(options)
    dateStr = dateStr.replace(",", "");
    dateStr = dateStr.substr(0, dateStr.length-6);
    return dateStr;
}

// Cheсk if the same request was made less than 5 minutes ago.If yes  we get the previous response and insert it.
function checkSameRequestPage(form) {

    var xhttpr = new XMLHttpRequest();
    var checkSameRequest = {
        "command": "check_same_request",
        "element": form.element.value,
        "url": form.url.value
    };
    var str = JSON.stringify(checkSameRequest);
    xhttpr.open("POST", "/colnect/counter.php", true);
    xhttpr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhttpr.send(str);
    xhttpr.onreadystatechange = function () {
        if (xhttpr.readyState == 4 && xhttpr.status == 200) {
            var checkSameRequest = JSON.parse(xhttpr.responseText);
            if (checkSameRequest.same_request == "url_not_available") {

                document.getElementById("error_message").innerHTML = "Url not available";

            } else if (checkSameRequest.same_request == "not_exist") {

                requestToPage(form, checkSameRequest);

            } else if (checkSameRequest.same_request == "exist") {
                var time = timeFormatting(checkSameRequest.time);
                var requestPageData = "URL " + form.url.value + " Fetched on ";
                requestPageData += time + ", took " + checkSameRequest.duration + " msec." + '<br>';
                requestPageData += "Element < " + form.element.value + " > appeared ";
                requestPageData += checkSameRequest.count_elements_page + " times in page.";

                document.getElementById("requestPage").innerHTML = requestPageData;

                getStatistic(form);
            }
        }
    }
}

// Find out the number of elements on the page and insert the statistical data in the html document.
// And send data about the requested HTML page to the server.
function requestToPage(form, checkSameRequest) {

// Find out the number of elements on the page
    var htmlPage = $('<div></div>');
    htmlPage.html(atob(checkSameRequest.content_page));

    var countElements = $(form.element.value, htmlPage).length;

    var time = timeFormatting(checkSameRequest.time);
    var requestPageData = "URL " + form.url.value + " Fetched on ";
    requestPageData += time + ", took " + checkSameRequest.duration + " msec." + '<br>';
    requestPageData += "Element < " + form.element.value + " > appeared ";
    requestPageData += countElements + " times in page.";

    document.getElementById("requestPage").innerHTML = requestPageData;

    var xhttpr = new XMLHttpRequest();
    var checkSameRequest = {
        "command": "check_request",
        "element": form.element.value,
        "url": form.url.value,
        "id_request": checkSameRequest.id_request,
        "count_elements_page": countElements
    };
    var str = JSON.stringify(checkSameRequest);
    xhttpr.open("POST", "/colnect/counter.php", true);
    xhttpr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhttpr.send(str);

    getStatisticForUrl(xhttpr);

}

function checkFormAndSameRequestPage(form) {
    document.getElementById("error_message").innerHTML = "";
    document.getElementById("requestPage").innerHTML = "";
    document.getElementById("statisticReport").innerHTML = "";
    if (checkForm(form)) {
        checkSameRequestPage(form);
    }
}



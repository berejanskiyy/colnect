<?php

include_once("util/Config.php");

class DB
{

    private $link;

    public function __construct()
    {
        $this->link = new PDO(Config::MYSQL_CONFIG, Config::MYSQL_USER, Config::MYSQL_PASSWORD);
    }

    /**
     * @param $url
     * @param $domain
     * @param $element
     * @param $time
     * @param $duration
     */
    public function addCheckedUrlInDb($url, $domain, $element, $id, $count_elements_page)
    {
        $url_id = $this->addUrl($url);
        $domain_id = $this->addDomain($domain);
        $element_id = $this->addElement($element);
        $this->getDataClient($domain_id, $url_id, $element_id, $count_elements_page, $id);

    }

    public function addTimeDuration($duration)
{
    $query = "INSERT INTO request(time, duration) VALUES (now(), ?);";
    $query_prepare = $this->link->prepare($query);
    $query_prepare->bindParam(1, $duration, PDO::PARAM_INT);

    $query_prepare->execute();
    $query_prepare->closeCursor();
    $id = $this->link->lastInsertId();

    return $id;
}

    /**
     * @param $domain_id
     * @param $url_id
     * @param $element_id
     * @param $time
     * @param $duration
     */
    public function getDataClient($domain_id, $url_id, $element_id, $count_elements_page, $id)
    {
        $query = "UPDATE request SET domain_id = ?, url_id=?, element_id=?, count_elements_page = ? WHERE id = ?;";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $domain_id, PDO::PARAM_INT);
        $query_prepare->bindParam(2, $url_id, PDO::PARAM_INT);
        $query_prepare->bindParam(3, $element_id, PDO::PARAM_INT);
        $query_prepare->bindParam(4, $count_elements_page, PDO::PARAM_INT);
        $query_prepare->bindParam(5, $id, PDO::PARAM_INT);
        $query_prepare->execute();
        $query_prepare->closeCursor();
    }

    /**
     * @param $id
     * @return array
     */
    public function getDataQueryById($id)
    {
        $query = "SELECT unix_timestamp(time) AS time,duration,count_elements_page FROM request WHERE id=?;";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $id, PDO::PARAM_INT);
        $query_prepare->execute();
        $dataQueryById = null;
        while ($row = $query_prepare->fetch(PDO::FETCH_OBJ)) {
            $dataQueryById = $row;
        }
        $query_prepare->closeCursor();
        return $dataQueryById;
    }

    /**
     * @param $url
     * @return string
     */
    public function addUrl($url)
    {
        $query = "INSERT IGNORE INTO url(name) VALUE (?);";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $url, PDO::PARAM_STR);

        $query_prepare->execute();
        $query_prepare->closeCursor();
        $id = $this->link->lastInsertId();
        if ($id == 0) {

            return $this->getUrlId($url);
        }

        return $id;
    }

    /**
     * @param $url
     * @return string
     */
    public function getUrlId($url)
    {
        $query = "SELECT id FROM url WHERE url.name=?;";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $url, PDO::PARAM_STR);

        $query_prepare->execute();
        $id = "";
        while ($row = $query_prepare->fetch()) {

            $id = $row['id'];
        }
        $query_prepare->closeCursor();
        return $id;
    }

    /**
     * @param $domain
     * @return string
     */
    public function addDomain($domain)
    {
        $query = "INSERT IGNORE INTO domain(name) VALUE (?);";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $domain, PDO::PARAM_STR);

        $query_prepare->execute();
        $query_prepare->closeCursor();
        $id = $this->link->lastInsertId();
        if ($id == 0) {

            return $this->getDomainId($domain);
        }

        return $id;
    }

    /**
     * @param $domain
     * @return string
     */
    public function getDomainId($domain)
    {
        $query = "SELECT id FROM domain WHERE domain.name=?;";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $domain, PDO::PARAM_STR);

        $query_prepare->execute();
        $id = "";
        while ($row = $query_prepare->fetch()) {

            $id = $row['id'];
        }
        $query_prepare->closeCursor();
        return $id;
    }

    /**
     * @param $element
     * @return string
     */
    public function addElement($element)
    {
        $query = "INSERT IGNORE INTO element(name) VALUE (?);";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $element, PDO::PARAM_STR);

        $query_prepare->execute();
        $query_prepare->closeCursor();
        $id = $this->link->lastInsertId();
        if ($id == 0) {
            return $this->getElementId($element);
        }

        return $id;
    }

    /**
     * @param $element
     * @return string
     */
    public function getElementId($element)
    {
        $query = "SELECT id FROM element WHERE element.name=?;";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $element, PDO::PARAM_STR);

        $query_prepare->execute();
        $id = "";
        while ($row = $query_prepare->fetch()) {

            $id = $row['id'];
        }
        $query_prepare->closeCursor();
        return $id;
    }



    /**
     * Count URLs of that domain have been checked till now
     * @param $domain
     * @return string
     */
    public function countCheckedUrlsOfDomain($domain)
    {
        $query = "SELECT count(DISTINCT request.url_id) AS urlsOfDomain
            FROM request
            INNER JOIN domain ON request.domain_id = domain.id
            WHERE domain.name = ?;";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $domain, PDO::PARAM_STR);
        $query_prepare->execute();
        $urlsOfDomain = "";
        while ($row = $query_prepare->fetch()) {
            $urlsOfDomain = $row['urlsOfDomain'];
        }
        $query_prepare->closeCursor();
        return $urlsOfDomain;
    }

    /**
     * Average page fetch time from that domain during the last 24 hours.
     * @param $domain
     * @return string
     */
    public function averagePageFetchTimeOfDomain($domain)
    {
        $PERIOD = 24 * 60 * 60;
        $query = " SELECT round(avg(request.duration)) AS averagePageFetchTime
            FROM request
            INNER JOIN domain ON request.domain_id = domain.id
            WHERE domain.name = ? AND (unix_timestamp() - request.time) < ?;";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $domain, PDO::PARAM_STR);
        $query_prepare->bindParam(2, $PERIOD, PDO::PARAM_INT);
        $query_prepare->execute();
        $averagePageFetchTime = "";
        while ($row = $query_prepare->fetch()) {
            $averagePageFetchTime = $row['averagePageFetchTime'];
        }
        $query_prepare->closeCursor();
        return $averagePageFetchTime;
    }

    /**
     * Total count of this element from this domain.
     * @param $domain
     * @param $element
     * @return string
     */
    public function countElementsOfDomain($domain, $element)
    {
        $query = "SELECT sum(request.count_elements_page) AS countElements
            FROM request
            INNER JOIN domain ON request.domain_id = domain.id
            INNER JOIN element ON request.element_id = element.id
            WHERE domain.name = ? AND element.name = ?;";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $domain, PDO::PARAM_STR);
        $query_prepare->bindParam(2, $element, PDO::PARAM_STR);
        $query_prepare->execute();
        $countElements = "";
        while ($row = $query_prepare->fetch()) {
            $countElements = $row['countElements'];
        }
        $query_prepare->closeCursor();
        return $countElements;
    }

    /**
     * Total count of this element from ALL requests ever made.
     * @param $element
     * @return string
     */
    public function countElementsOfRequests($element)
    {
        $query = "SELECT sum(request.count_elements_page) AS countElements
            FROM request
            INNER JOIN element ON element.id = request.element_id
            WHERE element.name = ?;";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $element, PDO::PARAM_STR);
        $query_prepare->execute();
        $countElements = "";
        while ($row = $query_prepare->fetch()) {
            $countElements = $row['countElements'];
        }
        $query_prepare->closeCursor();
        return $countElements;
    }

    /**
     * Same request was made less than 5 minutes ago
     * @param $url
     * @param $element
     * @return int
     */
    public function getIdSameRecentRequest($url, $element)
    {
        $FIVE_MINUTES = 5 * 60;
        $query = "SELECT max(request.id) as max_id 
            FROM request
              INNER JOIN element ON element.id = request.element_id
              INNER JOIN url ON url.id = request.url_id
            WHERE url.name = ? AND element.name = ? 
            AND  (unix_timestamp() - unix_timestamp(request.time)) < ? ";
        $query_prepare = $this->link->prepare($query);
        $query_prepare->bindParam(1, $url, PDO::PARAM_STR);
        $query_prepare->bindParam(2, $element, PDO::PARAM_STR);
        $query_prepare->bindParam(3, $FIVE_MINUTES, PDO::PARAM_INT);

        $query_prepare->execute();
        $id = -1;
        while ($row = $query_prepare->fetch()) {
            $id = $row['max_id'];
        }
        $query_prepare->closeCursor();
        if ($id == null) $id = -1;
        return $id;
    }

}
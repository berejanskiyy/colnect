<?php


class Statistic
{

    private $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    private function getDomain($url)
    {
        $componentsUrl = parse_url($url);
        return $componentsUrl['host'];
    }


    public function setClientData($url, $element, $id, $count_elements_page)
    {
        $domain = $this->getDomain($url);

        // Add result of checkForm on another page into database
        $this->db->addCheckedUrlInDb($url, $domain, $element, $id, $count_elements_page);
    }

    public function clientsStatistic($element, $url)
    {
        $domain = $this->getDomain($url);
        // Count URLs of that domain have been checked till now
        $checkedUrlsOfDomain = $this->db->countCheckedUrlsOfDomain($domain);

        // Average page fetch time from that domain during the last 24 hours.
        $averagePageFetchTime = $this->db->averagePageFetchTimeOfDomain($domain);

        // Total count of this element from this domain.
        $totalCountElementsDomain = $this->db->countElementsOfDomain($domain, $element);

        // Total count of this element from ALL requests ever made.
        $totalCountElements = $this->db->countElementsOfRequests($element);

        $statisticForPage = new stdClass();

        $statisticForPage->countCheckedUrlsOfDomain = $checkedUrlsOfDomain;
        $statisticForPage->averagePageFetchTime = $averagePageFetchTime;
        $statisticForPage->totalCountElementsDomain = $totalCountElementsDomain;
        $statisticForPage->totalCountElements = $totalCountElements;
        $statisticForPage->domain = $domain;
        $statisticForPage->element = $element;

        return $statisticForPage;
    }

    private function getPage($url) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.07 Safari/537.36'
        ));
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    // Get web page load data
    public function checkSameRequest($url, $element)
    {
        // Cheсk if the same request was made less than 5 minutes ago.
        $id = $this->db->getIdSameRecentRequest($url, $element);

        if ($id == -1) { // If there was no similar request to the site
            $timeBeforeRequestMsec = round(microtime(true) * 1000);// Get time before requesting a website
            $checkSameRequest = new stdClass;
            try {
                // Get web site content
                $pageContent = $this->getPage($url);

                if ($pageContent === false) {

                    $checkSameRequest->same_request = "url_not_available";
                    return $checkSameRequest;

                } else {
                    // Get time after requesting a website in milliseconds
                    $timeAfterRequestMsec = round(microtime(true) * 1000);

                    // Query time
                    $duration = $timeAfterRequestMsec - $timeBeforeRequestMsec;

                    $id_request = $this->db->addTimeDuration($duration);

                    $checkSameRequest->same_request = "not_exist";
                    $checkSameRequest->duration=$duration;
                    $checkSameRequest->time = round($timeAfterRequestMsec/1000);
                    $checkSameRequest->id_request = $id_request;
                    $checkSameRequest->content_page = base64_encode($pageContent);

                    return $checkSameRequest;

                }
            } catch (Exception $e) {

                $checkSameRequest->same_request = "url_not_available";
                //echo json_encode($e);
                return $checkSameRequest;
            }

        } else { // If similar request have been done
            $dataQueryById = $this->db->getDataQueryById($id);
            $checkSameRequest = new stdClass;
            $checkSameRequest->same_request = "exist";
            $checkSameRequest->time = $dataQueryById->time;
            $checkSameRequest->duration = $dataQueryById->duration;
            $checkSameRequest->count_elements_page = $dataQueryById->count_elements_page;

            return $checkSameRequest;

        }
    }


}
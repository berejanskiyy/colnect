CREATE TABLE request (
  id                  INT AUTO_INCREMENT PRIMARY KEY,
  domain_id           INT,
  url_id              INT,
  element_id          INT,
  time                DATETIME,
  duration            INT,
  count_elements_page INT
);

CREATE TABLE domain (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) UNIQUE
);

CREATE TABLE url (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(150) UNIQUE
);

CREATE TABLE element (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) UNIQUE
);